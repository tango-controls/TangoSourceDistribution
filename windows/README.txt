TANGO binary release for Windows - README
=========================================

This is the TANGO binary release for Windows. TANGO is a toolkit for building
object oriented control systems based on CORBA and Zmq. TANGO is a joint effort
of the Tango community.

In the case of upgrading an already running Tango system, please follow
the instructions in the chapter UPGRADING FROM PREVIOUS TANGO RELEASES.

WHAT'S INSIDE
-------------

This binary release contains :

(1) The omniORB, Zmq and libjpeg dependencies for the Tango C++ library

(2) The Tango C++ libraries and the java packages

(3) The Tango database device server

(4) The starter device server and the astor administration application

(5) The Jive application.

(6) The Pogo application.

(7) A test device server called TangoTest with its source files

(8) The atk graphical toolkit for writing tango applications in java

(9) The atkpanel application as example of using atk

(10) The atktuning application for tuning attributes in a device

(11) The logviewer application for visualising logging messages

(12) The atkmoni application to monitor scalar attribute values in time

(13) The jdraw synoptic editor to create synoptic applications

(14) The synopticAppli to run and test synoptics created with jdraw

(15) The Tango controlled access device server.

     It is used if you want to run Tango with its controlled access feature.

Version information for these components can be found in the accompanying
versions.txt file.

Included in the package is also the documentation for Tango rendered as html
from tango-doc.

The Jive application is a Tango database browsing tool written in Java. It also
allows device testing. Its documentation is at

  https://jive.readthedocs.io

The Astor application is also a Java application. With the help of the Tango
starter device server, it allows full remote Tango control system administration.
Its documentation is at :

  https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/astor/index.html

The Pogo application is a graphical Tango device server code generator.
Its documentation is at :

  https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/pogo/index.html

tg_devtest is a device testing application for device server which do not
use the database. It is a sub-part of Jive.

The command line tools (tg_d*) include various command line tools to access tango devices, servers and database. See command line tool subsection.


PREREQUISITES
-------------

Before installing TANGO you need to install :

(1) mysql server available from http://mysql.org (version >= 5.5)
(2) Java 17

INSTALLING
----------

(1) Run the setup program

(2) Define a TANGO_HOST environment variable containing both the name of the host
    on which you plan to run the TANGO database device server (e.g. myhost) and
    its associated port (e.g. 20000). With the given example, the TANGO_HOST
    should contain myhost:20000

(3) Check a TANGO_ROOT environment variable is set. (The installer sets the
    variable during the installation.) The TANGO_ROOT environment variable
    should contain the root directory where Tango has been installed. If you
    haven't changed anything during the installation procedure, it should be
    "C:\Program Files\tango"

RUNNING
-------

To test whether the TANGO installation worked, do the following:

(1) create the TANGO database tables and register some devices

    If you do not define any environment variable, the mysql
  default user will be used. Please, check that this default user
  has the rights to create database, tables...
  You can also create the environment variables
  MYSQL_USER and MYSQL_PASSWORD to define which user will be used
  by the script to connect to the MySQL database (preferred solution)

    1.1 be sure the mysql server is running and that you have the mysql cmd in your PATH
    1.2 Open a "cmd" window and cd to %TANGO_ROOT%\share\tango\db
    1.3 execute create_db.bat

(2) start the TANGO database:

    2.1 execute %TANGO_ROOT%\bin\start-db.bat

	 Specify the mysql account to be used by the database server.
	 Two possibilities are available:
    - set the following environment variables to the account and
      password to be used. This is already done when you have
      entered the mysql user and password during the tango
      installation.

      MYSQL_USER to specify the mysql login name
      MYSQL_PASSWORD to specify the password

    - configure the mysql client with the mysql configuration
      file my.cnf

      [client]
      user      = [mysql login name]
      password  = [password]

    2.2 MySQL library path (<MySQL installation path>\lib) needs to be included in your PATH.
    2.3 The Tango databse requires Microsoft Visual C++ 2013 Redistributable
Package to run.  This is provided by the Tango Installer.

(3) start the test device server tangotest:

    %TANGO_ROOT%\bin\start-tangotest.bat test

(4) start jive by typing:

    %TANGO_ROOT%\bin\start-jive.bat

(5) test your device using the test device in jive

(6) try to compile the TangoTest device

    Example:Using cmake from Visual Studio C++ 2019 Developer Command Prompt

      cd %TANGO_ROOT%/cppserver/tangotest
      cmake -G"Visual Studio 16 2019" -A"x64" -Bbuild -S.
      cmake --build build

(7) write new device servers using pogo:

    %TANGO_ROOT%\bin\start-pogo.bat


UPDATING DATABASE FROM PREVIOUS TANGO RELEASES
-------

UPDATING FROM A TANGO 9.2.5 DATABASE
--------------------------------------

Tango 9.3.4 includes an update of the database:
(1) new DServer always allowed command: EventConfirmSubscription,
(2) new entry for TangoRestServer in device table.

To update your database, follow these instructions (assuming you have the mysql cmd in your PATH):

a - Stop your control system and your database server(s)

b - Backup your database (Recommended, not mandatory)

c - Cd to the %TANGO_ROOT%\share\tango\db directory

d - Run the update script:
	mysql -u[user] -p[password] < ./update_db_from_9.2.5_to_9.3.4.sql

e - Restart your database server(s)


UPDATING FROM A TANGO 9 (< 9.2.5) DATABASE
--------------------------------------

Tango 9.3.4 includes an update of the database:
(1) new DServer always allowed command: EventConfirmSubscription,
(2) new entry for TangoRestServer in device table.
(3) update of the stored procedure (release 1.13).

To update your database, follow these instructions (assuming you have the mysql cmd in your PATH):

a - Stop your control system and your database server(s)

b - Backup your database (Recommended, not mandatory)

c - Cd to the %TANGO_ROOT%\share\tango\db directory

d - Run the update script to update the stored procedure:
	update_db.bat

e - Run the update script for the new entries (1) and (2) in the DB:
	mysql -u[user] -p[password] < ./update_db_from_9.2.5_to_9.3.4.sql

f - Restart your database server(s)

UPGRADING from Tango 8 database
-------------------------------

Tango 9.3.4 includes an update of the database:
(1) new DServer always allowed command: EventConfirmSubscription.
(2) new entry for TangoRestServer in device table.
(3) update of the stored procedure (release 1.13).
(4) some new commands in the list of allowed commands for the Database class (for Tango Access Control system)
(5) some new tables related to the new pipe feature.

To update your database, follow these instructions (assuming you have the mysql cmd in your PATH):

a - Stop your control system and your database server(s)

b - Backup your database (Recommended, not mandatory)

c - Cd to the %TANGO_ROOT%\share\tango\db directory

d - Run the update script:
	mysql -u[user] -p[password] < ./update_db_from_8_to_9.3.4.sql

e - Restart your database server(s)


UPGRADING from Tango 7 database
-------------------------------

Tango 9.3.4 includes an update of the database:
(1) new DServer always allowed command: EventConfirmSubscription.
(2) new entry for TangoRestServer in device table.
(3) update of the stored procedure (release 1.13).
(4) some new commands in the list of allowed commands for the Database class (for Tango Access Control system)
(5) some new tables related to the new pipe feature.

To update your database, follow these instructions (assuming you have the mysql cmd in your PATH):

a - Stop your control system and your database server(s)

b - Backup your database (Recommended, not mandatory)

c - Cd to the %TANGO_ROOT%\share\tango\db directory

d - Run the update script:
	mysql -u[user] -p[password] < ./update_db_from_7_to_9.3.4.sql

e - Restart your database server(s)


If you do not define any environment variable, the mysql
default user will be used. Please, check that this default user
has the rights to create database, tables...
You can also create the environment variables
MYSQL_USER and MYSQL_PASSWORD to define which user will be used
by the script to connect to the MySQL database (recommended solution)


NOTES
-----

Entry for the starter device server is created in the database as
"starter/local-starter".
In the Jive Server node, rename its instance name to your host name
Entry for its device is also created in the database as
"tango/admin/local-starter".
In the Jive server node, rename this device to "tango/admin/<your host name>"

The Starter on Windows has a limitation. It is NOT allowed to define its path to find
device server process executable as a folder with name containing space (ie C:\Program Files\....)

The Access Control hasn't been tested.

REMARKS
-------

TANGO 9.1:
This is a major release. Recompiling a device server with Tango 9.1
means recompiling ALL classes of the device server. You need to work with
coherent include files.

TANGO 8.0:
This is a major release. Recompiling a device server with Tango 8.0
means recompiling ALL classes of the device server. You need to work with
coherent include files.

TANGO 7.0:
This is a major release. Recompiling a device server with Tango 7.0
means recompiling ALL classes of the device server. You need to work with
coherent include files.

TANGO 6.1:
The database server in this package uses stored procedures. To make it reliably
work the MySQL version must be >5.0.
To exploit the full performance of the new database server, on an already
installed Tango system, the the chapter UPDATING FROM PREVIOUS TANGO RELEASES.

TANGO 6.0:
Tango 6.0 is a major release, because it uses omniORB 4.1 which is
a major release of omniORB. The code generated from the with the IDL
compiler is not compatible with the code gernerated with omniORB 4.0.x!
To avoid any confusion with new and old libraries we decided to
make also a major Tango release.

Recompiling a device server with Tango 6.0 and omniORB 4.1 means
recompiling ALL classes of the device server. You need to work with
coherent include files.

TANGO 5.5.2:
The new database server keeps by default a history of the last 10 changes
for every property value. To configure another history depth, the property
"historyDepth" of the database device can be used.
See the README.txt file in share/tango/db for more information.
With Jive it is possible to view the history of a property value or to
restore an old value. Go to Tools menu and choose "Database history".
To view property values, the wildcard * can be used.

TANGO 5.5:
The new version of the starter server implements a better sequencing for
starting Tango server processes and introduces a MOVING state to
indicate the running start-up sequence. The included version of the
Astor application handles this new state.

When using events with Tango, two diagnostic tools are available.
The astor application allows to configure and test the available
events. Under the view menu open the Device Browser. Choose and open
your device. Right click on an attribute to subscribe and configure
events for this attribute.
The atkpanel applications offers under the view menu a diagnostic
panel which shows the availability of events and some statistics.

COMMAND LINE TOOLS
------------------

Make a diff between a property file and the DB

>tg_dbdiff test.res
test.res : matches


Output all properties of a Tango server

>tg_dbview SinusGen/1
#---------------------------------------------------------
# CLASS SinusGen properties
#---------------------------------------------------------

CLASS/SinusGen->Description: "A Class to generate sinus curve."
CLASS/SinusGen->InheritedFrom: TANGO_BASE_CLASS
CLASS/SinusGen->ProjectTitle: "Sinus wave generator"

#---------------------------------------------------------
# SERVER SinusGen/1, SinusGen device declaration
#---------------------------------------------------------

SinusGen/1/DEVICE/SinusGen: "jlp/test/1"


# --- jlp/test/1 properties

jlp/test/1->polled_attr: state,\
                         3000
jlp/test/1->toto: 1

# --- jlp/test/1 attribute properties

jlp/test/1/Att_six->display_unit: 1.0
jlp/test/1/Att_six->min_value: 11
jlp/test/1/Att_six->standard_unit: 1.0
jlp/test/1/att_trois->display_unit: 1.0
jlp/test/1/att_trois->max_value: 100.0
jlp/test/1/att_trois->min_value: -10.0
jlp/test/1/att_trois->standard_unit: 1.0


# --- dserver/SinusGen/1 properties

dserver/SinusGen/1->polling_threads_pool_conf: "jlp/test/1"


Load a property file into the Database

>tg_dbupdate test.res


Ping device (wildcard are allowed)

>tg_devping jlp/test/1
jlp/test/1 is alive  (1 ms)
jlp/test/1 is alive  (0 ms)
jlp/test/1 is alive  (0 ms)


State device (wildcard are allowed)

>tg_devstate jlp/test/1
jlp/test/1 is OFF


Status device (wildcard are allowed)

>tg_devstatus jlp/test/1
jlp/test/1 - The state is OFF


Server/Starter command (start/stop/restart/ping)

>tg_server ubuntu20acu SinusGen/1 ping
SinusGen/1 is alive


Unexport device

>tg_devunexport jlp/test/1
jlp/test/1 unexported !


Device info

>tg_devinfo jlp/test/1
Device:          jlp/test/1
type_id:         IDL:Tango/Device_5:1.0
iiop_version:    1.2
host:            ubuntu20acu.esrf.fr (160.103.10.146)
alternate addr.: 172.18.0.1
alternate addr.: 172.17.0.1
alternate addr.: 192.168.122.1
port:            55843
Server:          SinusGen/1
Server PID:      4131583
Exported:        true
last_exported:   16th July 2024 at 08:54:17
last_unexported: ?


Device polling info

>tg_devpoll jlp/test/1
command  State
Polling period = 3000ms
Last record takes 20.095 ms
Data not updated since 21 mS
Delta between last records (in mS) = 3000, 3000, 2999, 3000

Drifts (ms):   0, 0, -1, 0,


DOCUMENTATION
-------------

Don't forget to READ THE MANUAL (in doc/tango.pdf) !

QUESTIONS
---------

You will definitely have some ! Send questions to info@tango-controls.org or on the
forum: http://www.tango-controls.org/community/forum

URL
---

Visit the TANGO website http://www.tango-controls.org for online
documentation, news and to download add-on packages.

CHANGES
-------

See the file TANGO_CHANGES to get an overview of all modification
between the different Tango versions.
