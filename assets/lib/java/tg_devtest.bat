@ECHO OFF

IF NOT DEFINED TANGO_ROOT (
 ECHO TANGO_ROOT is not defined. Aborting!
 ECHO Please define a TANGO_ROOT env. var. pointing to your TANGO install directory.
 PAUSE
 GOTO SCRIPT_END
)

IF NOT DEFINED TANGO_HOST (
 ECHO TANGO_HOST is not defined. Aborting!
 ECHO Please define a TANGO_HOST env. var. pointing to your TANGO database.
 ECHO TANGO_HOST syntax is tango_database_host::tango_database_port [e.g. venus::20000].
 PAUSE
 GOTO SCRIPT_END
)

set LOGBACK=%TANGO_ROOT%\share\tango\logback.xml
set TANGO_JAVA_ROOT=%TANGO_ROOT%\share\java
set CP=%TANGO_JAVA_ROOT%\JTango.jar
set CP=%CP%;%TANGO_JAVA_ROOT%\ATKCore.jar
set CP=%CP%;%TANGO_JAVA_ROOT%\ATKWidget.jar
set CP=%CP%;%TANGO_JAVA_ROOT%\Jive.jar

java -DTANGO_HOST=%TANGO_HOST% -Dlogback.configurationFile="%LOGBACK%" -cp "%CP%" jive.ExecDev %*

:SCRIPT_END
