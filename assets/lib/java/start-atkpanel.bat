@ECHO OFF

GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: start-atkpanel.bat
# Author:   N.leclercq
# Date:     04/25/2003
# Purpose:  Launch ATKPanel
#
######################################################################
:SCRIPT_BEGIN

IF NOT DEFINED TANGO_ROOT (
 ECHO TANGO_ROOT is not defined. Aborting!
 ECHO Please define a TANGO_ROOT env. var. pointing to your TANGO install directory.
 PAUSE
 GOTO SCRIPT_END
)

CALL "%TANGO_ROOT%\bin\common.bat"

start javaw -DTANGO_HOST=%TANGO_HOST% atkpanel.MainPanel

:SCRIPT_END
