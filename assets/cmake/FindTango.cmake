if (DEFINED Tango_FOUND)
    return()
endif()
set(Tango_FOUND TRUE CACHE INTERNAL "")

message(STATUS "Using bundled libtango")
