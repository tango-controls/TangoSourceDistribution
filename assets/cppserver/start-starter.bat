@ECHO OFF

GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: start-starter.bat
# Author:   N.leclercq
# Date:     04/25/2003
# Purpose:  Launch the Starter device server
#
######################################################################
:SCRIPT_BEGIN

IF {%1} == {} (
 ECHO Error: no instance name specified.
 GOTO USAGE
)

IF NOT DEFINED TANGO_ROOT (
 ECHO TANGO_ROOT is not defined. Aborting!
 ECHO Please define a TANGO_ROOT env. var. pointing to your TANGO install directory.
 PAUSE
 GOTO SCRIPT_END
)

CALL "%TANGO_ROOT%\bin\common.bat"

starter %1 %2

GOTO SCRIPT_END

:USAGE

   ECHO Usage: start-starter.bat instance_name  [-vverbose_level]
   ECHO   instance_name: the server instance name (as registered in the TANGO database)
   ECHO   verbose_level: optional verbose level (e.g. -v1 for info, -v4 for debug)
   PAUSE

:SCRIPT_END
